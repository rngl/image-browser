﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Controls.Primitives;

namespace ImageBrowser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string[] fileList;
        private ScaleTransform ts = new ScaleTransform(1,1,0.5,0.5);
        private Border previousThumbnail = null; // for resetting button background colour

        private double mousePositionX = 0;
        private double mousePositionY = 0;
        private double hOffset = 0;
        private double vOffset = 0;
        private double beforeFullscreenModeScale;

        private PopupWindow popupWindow;

        public MainWindow()
        {
            InitializeComponent();
            MainImage.LayoutTransform = ts;
            UpdateLayout();
            GenerateFolderTree();
        }


        private void LoadFolderImages(string path)
        {
            try
            {
                fileList = null;
                fileList = Directory.EnumerateFiles(path, "*.*").Where(s => 
                    s.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase) ||
                    s.EndsWith(".jpeg", StringComparison.OrdinalIgnoreCase) ||
                    s.EndsWith(".gif", StringComparison.OrdinalIgnoreCase) ||
                    s.EndsWith(".bmp", StringComparison.OrdinalIgnoreCase)
                    ).ToArray();

                DeleteThumbnails();
                GenerateThumbnails();
            }
            catch
            {

            }
        }

        private void GenerateFolderTree()
        {
            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach(DriveInfo drive in drives)
            {
                TreeViewItem newTreeViewItem = new TreeViewItem
                {
                    Tag = drive.Name, // full path in tag
                    Header = drive.Name
                };
                newTreeViewItem.Selected += TreeViewItemSelectedEvent;
                newTreeViewItem.Expanded += TreeViewExpandedEvent;
                AddSubFolders(newTreeViewItem);
                DirectoryTree.Items.Add(newTreeViewItem);
            }
        }
        

        private void AddSubFolders(TreeViewItem treeViewItem)
        {
            if (!treeViewItem.HasItems)
            {
                try
                {
                    String[] directories = System.IO.Directory.GetDirectories(treeViewItem.Tag.ToString());
                    foreach (String s in directories)
                    {
                        // remove \ from the beginning of certain folder names
                        String header = s.ElementAt(treeViewItem.Tag.ToString().Length) == '\\' ? 
                            s.Substring(treeViewItem.Tag.ToString().Length + 1) : s.Substring(treeViewItem.Tag.ToString().Length);

                        TreeViewItem newTreeViewItem = new TreeViewItem
                        {
                            Tag = s,
                            Header = header
                        };

                        newTreeViewItem.Selected += TreeViewItemSelectedEvent;
                        newTreeViewItem.Expanded += TreeViewExpandedEvent;
                        treeViewItem.Items.Add(newTreeViewItem);
                    }
                }
                catch
                {

                }
            }
        }

        private void TreeViewExpandedEvent(object sender, RoutedEventArgs e)
        {
            foreach (TreeViewItem treeViewItem in ((TreeViewItem)sender).Items)
            {
                AddSubFolders(treeViewItem);
            }
        }


        private void TreeViewItemSelectedEvent(object sender, RoutedEventArgs e)
        {
            AddSubFolders((TreeViewItem) sender);
            LoadFolderImages(((TreeViewItem)sender).Tag.ToString());
            e.Handled = true;
        }


        // https://stackoverflow.com/questions/16234522/scrollviewer-mouse-wheel-not-working
        private void TreeViewMouseWheelEvent(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }


        private async Task SetMainWindowImage(string imageFile)
        {
            MainImage.Source = await Task.Run(() => CreateThumbnail(imageFile, null));
        }


        private void DeleteThumbnails()
        {
            ThumbnailGrid.Children.Clear();
            ThumbnailGrid.RowDefinitions.Clear();
        }


        private async void GenerateThumbnails()
        {
            for (int i = 0; i < fileList.Length; i++)
            {
                if (i % 2 == 0)
                {
                    ThumbnailGrid.RowDefinitions.Add(new RowDefinition());
                }

                Image newImage = new Image
                {
                    Margin = new Thickness(5, 5, 5, 5)
                };

                Border newBorder = new Border
                {
                    BorderThickness = new Thickness(0, 0, 1, 1),
                    BorderBrush = Brushes.Gray,
                    Background = Brushes.Transparent,
                    Child = newImage,
                    Tag = i,
                    IsHitTestVisible = true
                };

                newBorder.MouseLeftButtonDown += ThumbnailClickAction;
                Grid.SetColumn(newBorder, i % 2);
                Grid.SetRow(newBorder, i / 2);
                ThumbnailGrid.Children.Add(newBorder);

                newImage.Source = await Task.Run(() => CreateThumbnail(fileList[i], 200));
            }
        }


        // https://stackoverflow.com/questions/44846767/loading-images-into-a-grid-asynchronously
        private static BitmapImage CreateThumbnail(string imagePath, int? width)
        {
            var bitmap = new BitmapImage();

            using (var stream = new FileStream(imagePath, FileMode.Open, FileAccess.Read))
            {
                bitmap.BeginInit();
                if (width != null)
                    bitmap.DecodePixelWidth = (int) width;
                
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.StreamSource = stream;
                bitmap.EndInit();
            }
            
            bitmap.Freeze();
            return bitmap;
        }


        private async void ThumbnailClickAction(object sender, MouseButtonEventArgs e)
        {
            if (previousThumbnail != null)
                previousThumbnail.Background = Brushes.Transparent;
            previousThumbnail = (Border) sender;
            previousThumbnail.Background = Brushes.LightBlue;
            int thumbnailNumber = (int)previousThumbnail.Tag;
            await SetMainWindowImage(fileList[thumbnailNumber]);
            MainWindowImageFit(null, null);
            this.Title = (thumbnailNumber + 1) + "/" + fileList.Length + "  - " + fileList[thumbnailNumber];
        }


        private void MainImageMouseLeftDownEvent(object sender, MouseButtonEventArgs e)
        {
            mousePositionX = e.GetPosition(MainImageScrollviewer).X;
            mousePositionY = e.GetPosition(MainImageScrollviewer).Y;
            hOffset = MainImageScrollviewer.HorizontalOffset;
            vOffset = MainImageScrollviewer.VerticalOffset;
            e.Handled = true;
        }


        private void MainImageMouseMoveEvent(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                MainImageScrollviewer.ScrollToHorizontalOffset(hOffset + 1 * (mousePositionX - e.GetPosition(MainImageScrollviewer).X));
                MainImageScrollviewer.ScrollToVerticalOffset(vOffset + 1 * (mousePositionY - e.GetPosition(MainImageScrollviewer).Y));
            }
            e.Handled = true;
        }


        private void MainImageZoomEvent(object sender, MouseWheelEventArgs e)
        {
            ts.ScaleX += (e.Delta > 0) ? 0.2 : (ts.ScaleX > 0.25) ? -0.2 : 0;
            ts.ScaleY += (e.Delta > 0) ? 0.2 : (ts.ScaleY > 0.25) ? -0.2 : 0;
            e.Handled = true;
        }

        // https://social.msdn.microsoft.com/Forums/vstudio/en-US/e20a2433-6d6f-4bb7-af8f-d7f8ce5a7851/layouttransform-not-taking-mouse-location-as-center-for-zoomin-or-zoomout-?forum=wpf
        void scrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            ScrollViewer scrollViewer = sender as ScrollViewer;

            if (e.ExtentHeightChange != 0 || e.ExtentWidthChange != 0)
            {

                double xMousePositionOnScrollViewer = Mouse.GetPosition(scrollViewer).X;
                double yMousePositionOnScrollViewer = Mouse.GetPosition(scrollViewer).Y;
                double offsetX = e.HorizontalOffset + xMousePositionOnScrollViewer;
                double offsetY = e.VerticalOffset + yMousePositionOnScrollViewer;

                double oldExtentWidth = e.ExtentWidth - e.ExtentWidthChange;
                double oldExtentHeight = e.ExtentHeight - e.ExtentHeightChange;

                double relx = offsetX / oldExtentWidth;
                double rely = offsetY / oldExtentHeight;

                offsetX = Math.Max(relx * e.ExtentWidth - xMousePositionOnScrollViewer, 0);
                offsetY = Math.Max(rely * e.ExtentHeight - yMousePositionOnScrollViewer, 0);

                ScrollViewer scrollViewerTemp = sender as ScrollViewer;
                scrollViewerTemp.ScrollToHorizontalOffset(offsetX);
                scrollViewerTemp.ScrollToVerticalOffset(offsetY);
            }
        }


        // for popup to call
        public void CallMainWindowImageFullscreen()
        {
            MainWindowImageFullscreen(null, null);
        }

        private void MainImageRightClickEvent(object sender, MouseButtonEventArgs e)
        {
            MainWindowImageFullscreen(null, null);
            e.Handled = true;
        }

        // Fullscreen image
        private void MainWindowImageFullscreen(object sender, RoutedEventArgs e)
        {
            if (popupWindow == null)
            {
                beforeFullscreenModeScale = ts.ScaleX;
                ts.ScaleX = 1;
                ts.ScaleY = 1;
                popupWindow = new PopupWindow(this);
                maingrid.Children.Remove(MainImageBorder);
                popupWindow.GetChildBorder().Child = MainImageBorder;
                popupWindow.Owner = Window.GetWindow(this);
                //popupWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                popupWindow.Show();
            }
            else
            {
                popupWindow.GetChildBorder().Child = null;
                maingrid.Children.Add(MainImageBorder);
                popupWindow.Close();
                popupWindow = null;
                ts.ScaleX = beforeFullscreenModeScale;
                ts.ScaleY = beforeFullscreenModeScale;
            }
        }


        // reset zoom and location of main window image
        private void MainWindowImageReset(object sender, RoutedEventArgs e)
        {
            ts.ScaleX = 1;
            ts.ScaleY = 1;
            UpdateLayout();
            MainImageScrollviewer.ScrollToVerticalOffset(MainImageScrollviewer.ScrollableHeight / 2);
            MainImageScrollviewer.ScrollToHorizontalOffset(MainImageScrollviewer.ScrollableWidth / 2);
        }

        
        private void MainWindowImageFit(object sender, RoutedEventArgs e)
        {
            MainWindowImageReset(sender, e); // 
            FitMainImage();
        }


        // scale image to fit current visible area
        private void FitMainImage()
        {
            double ymod =  MainImageScrollviewer.ActualHeight / MainImage.ActualHeight;
            double xmod = MainImageScrollviewer.ActualWidth / MainImage.ActualWidth;
            ts.ScaleY = Math.Min(xmod, ymod);
            ts.ScaleX = Math.Min(xmod, ymod);
            UpdateLayout();
        }
    }
}
