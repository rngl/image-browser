﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ImageBrowser
{
    /// <summary>
    /// Interaction logic for popupWindow.xaml
    /// </summary>
    public partial class PopupWindow : Window
    {

        private MainWindow mainWindow;

        public PopupWindow(MainWindow _mainWindow)
        {
            InitializeComponent();
            this.mainWindow = _mainWindow;
        }

        public Border GetChildBorder()
        {
            return popupBorder;
        }

        public void CloseWindowAction(object sender, MouseButtonEventArgs e)
        {
            mainWindow.CallMainWindowImageFullscreen();
        }


        // http://mostlytech.blogspot.com/2008/01/maximizing-wpf-window-to-second-monitor.html
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Maximized;
            Visibility = Visibility.Visible;
        }

    }
}
